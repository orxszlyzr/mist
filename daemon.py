# -*- coding: utf-8 -*-
import time

from messageQueue import RabbitMQ
from tasks import send

consumer = RabbitMQ.consumer('localhost','guest','guest',5672)

consumer.register(send.do, 'messages', 'outgoing.*', exchange_type="topic")

while True:
    try:
        consumer.poll()
        time.sleep(1)
    except Exception,e:
        print e
    except:
        print "Exiting..."
        consumer.close()
        break
