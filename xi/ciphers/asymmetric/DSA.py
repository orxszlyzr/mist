# -*- coding: utf-8 -*-

import logging

from M2Crypto import DSA, BIO

from ..object import cipherProduct

log = logging.getLogger('Xi.ciphers.asymmetric.DSA')

class cipher:
    """Core DSA module, wrapped from OpenSSL using M2Crypto.
       DSA is only for signing.
       
       This module shall provide basic function as sign, verify, as well as
       signals of can_sign, can_encrypt, etc."""
    __privateKey = None
    __publicKey = None

    def __init__(self):
        pass

    def _getSignLimit(self):
        """Calculates limit on input when signing a text."""
        return int(self.bits * 0.75 / 8)

    def generate(self,**argv):
        """Generates new key pair.

           :argv bits  Specify DSA key length."""
        if argv.has_key('bits'):
            bits = argv['bits']
        else:
            bits = 4096
        if bits < 1024:
            raise Exception("Cannot accept such bits < 1024.")
        self.bits = bits

        log.debug("Generating a %d bits DSA key, please wait." % bits)

        self.__privateKey = DSA.gen_params(bits)
        self.__privateKey.gen_key()
        
        log.debug("DSA key generation done.")

        self._derive_pubkey()
        
    def sign(self, digest):
        """Return string of signed digest. Digest is not calculated in this
           function."""
        if self.__privateKey == None:
            return False
        signed = self.__privateKey.sign_asn1(digest)
        return signed

    def verify(self, digest, sign):
        """Verify a sign with this key, see if it matches given digest."""
        if self.__publicKey == None:
            return False
        try:
            return bool(self.__publicKey.verify_asn1(digest, sign))
        except Exception,e:
            pass
        return False

    def loadPublicKey(self, publickey):
        """Load serialized public key string."""

        # Try parse the public key info.
        try:
            if type(publickey) == str:
                j = cipherProduct(publickey)
            else:
                j = publickey
            if j.type != 'DSA_Public_Key':
                raise Exception("This is not a public key thus cannot be loaded.")
            pkdata = self._trim_keydata(j.data, False, False)
            self.bits = int(j.bits)
        except Exception,e:
            raise Exception("Failed loading publickey. Bad format. Error: %s" % e)

        # If parsable, Write down and load.
        try:
            membuff = BIO.MemoryBuffer()
            membuff.write(pkdata)
            self.__publicKey = DSA.load_pub_key_bio(membuff)
        except Exception,e:
            raise Exception("Cannot load public key: %s" % e)

        # Delete existing private key to avoid conflicts.
        self.__privateKey = None

        # Return True after load succeeded.
        return True

    def loadPrivateKey(self, privatekey):
        """Load private key in serialized string."""

        # Try parse the private key info.
        try:
            if type(privatekey) == str:
                j = cipherProduct(privatekey)
            else:
                j = privatekey
            if j.type != 'DSA_Private_Key':
                raise Exception("This is not a private key thus cannot be loaded.")
            pkdata = self._trim_keydata(j.data, True, False)
            self.bits = int(j.bits)
        except Exception,e:
            print str(e)
            raise Exception("Failed loading privatekey. Bad format.")

        # If parsable, Write down and load.
        try:
            membuff = BIO.MemoryBuffer()
            membuff.write(pkdata)
            self.__privateKey = DSA.load_key_bio(membuff)
        except Exception,e:
            raise Exception("Cannot load private key. Error: %s" % e)

        # Override existing public key.
        self._derive_pubkey()

        # Return True if succeeded.
        return True

    def getPublicKey(self, raw=False):
        """Get serialized public key string."""

        # Shall be used with a public key.
        if self.__publicKey == None:
            return False

        # Retrive pubkey data
        membuff = BIO.MemoryBuffer()
        self.__publicKey.save_pub_key_bio(membuff)
        pubkeydata = membuff.read_all()

        # Write down a good form of public key.
        pkinfo = cipherProduct()
        pkinfo.type = 'DSA_Public_Key'
        pkinfo.bits = self.bits
        pkinfo.data = self._trim_keydata(pubkeydata, False, True)
        if raw:
            return pkinfo
        return str(pkinfo)

    def getPrivateKey(self, raw=False):
        """Get serialized string of private key."""

        # Shall work with a private key.
        if self.__privateKey == None:
            return False

        # Retrive privatekey data
        membuff = BIO.MemoryBuffer()
        self.__privateKey.save_key_bio(membuff, None)
        prvkeydata = membuff.read_all()

        # Write down a good form of public key.
        pkinfo = cipherProduct()
        pkinfo.type = 'DSA_Private_Key'
        pkinfo.bits = self.bits
        pkinfo.data = self._trim_keydata(prvkeydata, True, True)
        if raw:
            return pkinfo
        return str(pkinfo)

    def _derive_pubkey(self):
        # derive DSA public key instance from self.__privateKey
        if self.__privateKey == None:
            return False
        membuff = BIO.MemoryBuffer()
        self.__privateKey.save_pub_key_bio(membuff)  #(filename)
        self.__publicKey = DSA.load_pub_key_bio(membuff)   #(filename)

    def _trim_keydata(self,data,isPrivate,operation):
        """This function removes PEM packaged key block's header and footer,
           and decode the base64 encoded data, or inverse. This is used to
           reduce data length."""

        if operation: # True: Trim the data
            if isPrivate:
                data = data[31:]   # -----BEGIN DSA PRIVATE KEY-----
                data = data[:-29]  # -----END DSA PRIVATE KEY-----
            else:
                data = data[26:]
                data = data[:-24]
            return data.strip().decode('base64')
        else: # False: Reconstruct the data
            data = data.encode('base64').strip()
            if isPrivate:
                data = "-----BEGIN DSA PRIVATE KEY-----\n%s\n-----END DSA PRIVATE KEY-----" % data
            else:
                data = "-----BEGIN PUBLIC KEY-----\n%s\n-----END PUBLIC KEY-----" % data
            return data
