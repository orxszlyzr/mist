# -*- coding: utf-8 -*-

import random
import logging

from M2Crypto import EC, BIO

from ..object import cipherProduct

log = logging.getLogger('Xi.ciphers.asymmetric.EC')

class cipher:
    _curves_name = {
        707:'NID_secp128r2',
        706:'NID_secp128r1',
        728:'NID_sect239k1',
        704:'NID_secp112r1',
        705:'NID_secp112r2',
        687:'NID_X9_62_c2pnb176v1',
        730:'NID_sect283r1',
        699:'NID_X9_62_c2pnb272w1',
        689:'NID_X9_62_c2tnb191v2',
        708:'NID_secp160k1',
        741:'NID_wap_wsg_idm_ecid_wtls8',
        716:'NID_secp521r1',
        736:'NID_wap_wsg_idm_ecid_wtls3',
        735:'NID_wap_wsg_idm_ecid_wtls1',
        739:'NID_wap_wsg_idm_ecid_wtls6',
        740:'NID_wap_wsg_idm_ecid_wtls7',
        703:'NID_X9_62_c2tnb431r1',
        727:'NID_sect233r1',
        721:'NID_sect163k1',
        738:'NID_wap_wsg_idm_ecid_wtls5',
        714:'NID_secp256k1',
        688:'NID_X9_62_c2tnb191v1',
        712:'NID_secp224k1',
        412:'NID_X9_62_prime239v1',
        413:'NID_X9_62_prime239v2',
        414:'NID_X9_62_prime239v3',
        737:'NID_wap_wsg_idm_ecid_wtls4',
        702:'NID_X9_62_c2pnb368w1',
        742:'NID_wap_wsg_idm_ecid_wtls9',
        731:'NID_sect409k1',
        685:'NID_X9_62_c2pnb163v2',
        710:'NID_secp160r2',
        709:'NID_secp160r1',
        684:'NID_X9_62_c2pnb163v1',
        693:'NID_X9_62_c2pnb208w1',
        722:'NID_sect163r1',
        723:'NID_sect163r2',
        700:'NID_X9_62_c2pnb304w1',
        719:'NID_sect131r1',
        720:'NID_sect131r2',
        715:'NID_secp384r1',
        732:'NID_sect409r1',
        411:'NID_X9_62_prime192v3',
        690:'NID_X9_62_c2tnb191v3',
        409:'NID_X9_62_prime192v1',
        694:'NID_X9_62_c2tnb239v1',
        695:'NID_X9_62_c2tnb239v2',
        696:'NID_X9_62_c2tnb239v3',
        724:'NID_sect193r1',
        725:'NID_sect193r2',
        701:'NID_X9_62_c2tnb359v1',
        726:'NID_sect233k1',
        717:'NID_sect113r1',
        718:'NID_sect113r2',
        743:'NID_wap_wsg_idm_ecid_wtls10',
        744:'NID_wap_wsg_idm_ecid_wtls11',
        745:'NID_wap_wsg_idm_ecid_wtls12',
        686:'NID_X9_62_c2pnb163v3',
        729:'NID_sect283k1',
        733:'NID_sect571k1',
        415:'NID_X9_62_prime256v1',
        410:'NID_X9_62_prime192v2',
        711:'NID_secp192k1',
        734:'NID_sect571r1',
        713:'NID_secp224r1',
        }
    _curves_id = {
        'NID_secp112r1':704,
        'NID_secp112r2':705,
        'NID_secp128r1':706,
        'NID_secp128r2':707,
        'NID_secp160k1':708,
        'NID_secp160r1':709,
        'NID_secp160r2':710,
        'NID_secp192k1':711,
        'NID_secp224k1':712,
        'NID_secp224r1':713,
        'NID_secp256k1':714,
        'NID_secp384r1':715,
        'NID_secp521r1':716,
        'NID_sect113r1':717,
        'NID_sect113r2':718,
        'NID_sect131r1':719,
        'NID_sect131r2':720,
        'NID_sect163k1':721,
        'NID_sect163r1':722,
        'NID_sect163r2':723,
        'NID_sect193r1':724,
        'NID_sect193r2':725,
        'NID_sect233k1':726,
        'NID_sect233r1':727,
        'NID_sect239k1':728,
        'NID_sect283k1':729,
        'NID_sect283r1':730,
        'NID_sect409k1':731,
        'NID_sect409r1':732,
        'NID_sect571k1':733,
        'NID_sect571r1':734,
        'NID_X9_62_prime192v1':409,
        'NID_X9_62_prime192v2':410,
        'NID_X9_62_prime192v3':411,
        'NID_X9_62_prime239v1':412,
        'NID_X9_62_prime239v2':413,
        'NID_X9_62_prime239v3':414,
        'NID_X9_62_prime256v1':415,
        'NID_X9_62_c2pnb163v1':684,
        'NID_X9_62_c2pnb163v2':685,
        'NID_X9_62_c2pnb163v3':686,
        'NID_X9_62_c2pnb176v1':687,
        'NID_X9_62_c2tnb191v1':688,
        'NID_X9_62_c2tnb191v2':689,
        'NID_X9_62_c2tnb191v3':690,
        'NID_X9_62_c2pnb208w1':693,
        'NID_X9_62_c2tnb239v1':694,
        'NID_X9_62_c2tnb239v2':695,
        'NID_X9_62_c2tnb239v3':696,
        'NID_X9_62_c2pnb272w1':699,
        'NID_X9_62_c2pnb304w1':700,
        'NID_X9_62_c2tnb359v1':701,
        'NID_X9_62_c2pnb368w1':702,
        'NID_X9_62_c2tnb431r1':703,
        'NID_wap_wsg_idm_ecid_wtls1':735,
        'NID_wap_wsg_idm_ecid_wtls3':736,
        'NID_wap_wsg_idm_ecid_wtls4':737,
        'NID_wap_wsg_idm_ecid_wtls5':738,
        'NID_wap_wsg_idm_ecid_wtls6':739,
        'NID_wap_wsg_idm_ecid_wtls7':740,
        'NID_wap_wsg_idm_ecid_wtls8':741,
        'NID_wap_wsg_idm_ecid_wtls9':742,
        'NID_wap_wsg_idm_ecid_wtls10':743,
        'NID_wap_wsg_idm_ecid_wtls11':744,
        'NID_wap_wsg_idm_ecid_wtls12':745,
    }
    _curves_signlimit = {
        13:(705,),
        14:(704,717,718,735,737,739,741),
        15:(707,),
        16:(706,719,720),
        20:(684,685,686,687,708,709,710,721,722,723,736,738,740,742),
        23:(688,689,690),
        24:(409,410,411,693,711,724,725),
        28:(712,713,745),
        29:(412,413,414,694,695,696,726,727,728,743,744),
        32:(415,699,714),
        35:(729,730),
        36:(700,),
        44:(701,702),
        48:(715,),
        50:(731,),
        51:(732,),
        52:(703,),
        65:(716,),
        71:(733,734),
    }

    __privateKey = None
    __publicKey = None
    __privateKey_curve = None
    __publicKey_curve = None

    def __init__(self):
        pass

    def _getSignLimit(self, curveid=False):
        if curveid == False:
            if self.__privateKey != None:
                curveid = self.__privateKey_curve
            elif self.__publicKey != None:
                curveid = self.__publicKey_curve
            else:
                raise Exception("Not initialized.")
        else:
            if type(curveid) == str:
                try:
                    curveid = self._curves_id[curveid]
                except:
                    raise Exception("Querying unknown curve's sign limit.")

        for limit in self._curves_signlimit:
            if curveid in self._curves_signlimit[limit]:
                return limit
        return min(self._curves_signlimit.keys())
        
    def generate(self,**argv):
        """Generate new EC key pair. Applicable parameter is `curve`."""

        # select EC curve.
        if argv.has_key('curve'):
            curve = argv['curve']
        else:
            curve = random.choice(self._curves_id.items())
            curve = curve[1]
        if not self._curves_name.has_key(curve):
            raise Exception("User desired impractical EC parameter.")
        curve_name = self._curves_name[curve]
        # generate a new EC instance, init both secret and public key instance.
        self.__privateKey = EC.gen_params(curve)
        self.__privateKey.gen_key()
        self._derive__publicKey()
        self.__privateKey_curve, self.__publicKey_curve = curve,curve
        
        log.debug("Generated new EC key basing on %s." % curve_name)

    def sign(self,digest):
        """Return string of signed digest. Digest is not calculated in this
           function."""
        if self.__privateKey == None:
            return False
        return self.__privateKey.sign_dsa_asn1(digest)

    def verify(self,digest,sign):        
        """Verify a sign with this key, see if it matches given digest."""
        if self.__publicKey == None:
            return False
        try:
            if self.__publicKey.verify_dsa_asn1(digest,sign):
                return True
        except Exception,e:
            print "Failed verifying signature: %s" % e
        return False

    def encrypt(self, message, encryptor):
        """Encrypt message of arbitary length using an `encryptor`.
           The encryptor must be some symmetric cipher. This function will
           random a key of max. possible length for this cipher's use."""

        # required working with a known public key.
        if self.__publicKey == None or self.__publicKey_curve == None: 
            return False

        # randomize a key
        tempkey = EC.gen_params(self.__publicKey_curve)
        tempkey.gen_key()
        sharedsecret = tempkey.compute_dh_key(self.__publicKey)
        
        log.debug("Length of key is: %d",(len(sharedsecret) * 8))

        # Encrypt
        ciphertext = encryptor(sharedsecret,message)
        # Get tempkey's public key.
        membuff = BIO.MemoryBuffer()
        tempkey.save_pub_key_bio(membuff)
        publickey = membuff.read_all()  #open(filename).read()
        # Return with serializer.
        ret = cipherProduct()
        ret.type = 'EC_Encrypted'
        ret.pubkey = publickey
        ret.ctext = ciphertext
        return ret

    def decrypt(self, ciphertext, decryptor):
        if self.__privateKey == None:
            return False
        try:
            if type(ciphertext) == str:
                j = cipherProduct(ciphertext)
            else:
                j = ciphertext
            if j.type != 'EC_Encrypted':
                raise Exception("Input may not be the intending ciphertext.")
            publickey = j.pubkey
            ciphertext= j.ctext
        except Exception,e:
            raise Exception("Bad EC ciphertext format.")
        try:
            # Read the temp. key. First write to a file.
            membuff = BIO.MemoryBuffer()
            membuff.write(publickey)
            tempkey = EC.load_pub_key_bio(membuff)
            # Combine this temp. key with our private key, and get Shared Secret.
            sharedsecret = self.__privateKey.compute_dh_key(tempkey)
        except Exception,e:
            raise Exception("Unable to load public key. Error is [%s]." % e)
        return decryptor(sharedsecret,ciphertext)

    def loadPublicKey(self,publickey):
        # Try parse the public key info.
        try:
            if type(publickey) == str:
                j = cipherProduct(publickey)
            else:
                j = publickey
            if j.type != 'EC_Public_Key':
                raise Exception("This is not a public key thus cannot be loaded.")
            if self._curves_id.has_key(j.curve):
                curve = self._curves_id[j.curve]
            else:
                raise Exception("Unrecognized EC curve specified.")
            pkdata = self._trim_keydata(j.data,False,False)
        except Exception,e:
            raise Exception("Failed loading publickey. Bad format. Error: %s" % e)
        # If parsable, Write down and load.
        try:
            membuff = BIO.MemoryBuffer()
            membuff.write(pkdata)
            self.__publicKey = EC.load_pub_key_bio(membuff)   #(filename)
            self.__publicKey_curve = curve
        except Exception,e:
            raise Exception("Cannot load public key: %s" % e)
        # Delete existing private key to avoid conflicts.
        self.__privateKey = None
        self.__privateKey_curve = None
        # succeeded.
        return True

    def loadPrivateKey(self,privatekey):
        # Try parse the private key info.
        try:
            if type(privatekey) == str:
                j = cipherProduct(privatekey)
            else:
                j = privatekey
            if j.type != 'EC_Private_Key':
                raise Exception("This is not a private key thus cannot be loaded.")
            if self._curves_id.has_key(j.curve):
                curve = self._curves_id[j.curve]
            else:
                raise Exception("Unrecognized EC curve specified.")
            pkdata = self._trim_keydata(j.data, True, False)
        except Exception,e:
            raise Exception("Failed loading privatekey. Bad format.")
        # If parsable, Write down and load.
        try:
            membuff = BIO.MemoryBuffer()
            membuff.write(pkdata)
            self.__privateKey = EC.load_key_bio(membuff)  #(filename)
            self.__privateKey_curve = curve
        except Exception,e:
            raise Exception("Cannot load private key. Error: %s" % e)
        # Override existing public key.
        self.__publicKey_curve = curve
        self._derive__publicKey()
        # succeeded.
        return True

    def getPublicKey(self, raw=False):
        """Returns serialized public key in this key pair."""
        if self.__publicKey == None or self.__publicKey_curve == None:
            return False
        # Retrive pubkey data
        membuff = BIO.MemoryBuffer()
        self.__publicKey.save_pub_key_bio(membuff)  #(filename)
        pubkeydata = membuff.read_all()  #open(filename).read()
        # Write down a good form of public key.
        pkinfo = cipherProduct()
        pkinfo.type = 'EC_Public_Key'
        pkinfo.curve = self._curves_name[self.__publicKey_curve]
        pkinfo.data = self._trim_keydata(pubkeydata,False,True)
        if raw:
            return pkinfo
        return str(pkinfo)

    def getPrivateKey(self, raw=False):
        if self.__privateKey == None or self.__privateKey_curve == None:
            return False
        # Retrive privatekey data
        membuff = BIO.MemoryBuffer()
        self.__privateKey.save_key_bio(membuff,None)
        prvkeydata = membuff.read_all()

        # Write down a good form of public key.
        pkinfo = cipherProduct()
        pkinfo.type = 'EC_Private_Key'
        pkinfo.curve = self._curves_name[self.__privateKey_curve]
        pkinfo.data = self._trim_keydata(prvkeydata,True,True)

        if raw:
            return pkinfo
        return str(pkinfo)

    def _derive__publicKey(self):
        # derive EC public key instance from self.__privateKey
        if self.__privateKey == None:
            return False
        membuff = BIO.MemoryBuffer()
        self.__privateKey.save_pub_key_bio(membuff)
        self.__publicKey = EC.load_pub_key_bio(membuff) 
    
    def _trim_keydata(self,data,isPrivate,operation):
        if operation: # True: Trim the data
            if isPrivate:
                data = data[30:]   # -----BEGIN EC PRIVATE KEY-----
                data = data[:-28]  # -----END EC PRIVATE KEY-----
            else:
                data = data[26:]   # -----BEGIN PUBLIC KEY-----
                data = data[:-24]  # -----END PUBLIC KEY------
            return data.strip().decode('base64')
        else: # False: Reconstruct the data
            data = data.encode('base64').strip()
            if isPrivate:
                data = "-----BEGIN EC PRIVATE KEY-----\n%s\n-----END EC PRIVATE KEY-----" % data
            else:
                data = "-----BEGIN PUBLIC KEY-----\n%s\n-----END PUBLIC KEY-----" % data
            return data
