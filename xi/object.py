# -*- coding: utf-8 -*-

import msgpack

from ciphers.hashes import Hash

class cipherProduct:
    """Abstract class for cipher products.
       
       This class records cipher result parameters, and provides functions as
       hashing and serializing."""

    __userDefined__ = {}

    def __init__(self, serializedString=None):
        """You may supply a serialized string during initialization.
           The instance will then be initialized with deserialized data."""
        if type(serializedString) == str:
            self.__userDefined__ = msgpack.unpackb(serializedString,
                                                   use_list=False)

    def __getattr__(self, name):
        """Return user defined attributes. These attributes are stored in
           self.__userDefined__"""
        if self.__userDefined__.has_key(name):
            return self.__userDefined__[name]
        else:
            return False

    def __setattr__(self, name, value):
        """Set user defined attributes. This class is designed to have all
           attributes stored as user defined, except the `__userDefined__`, so
           we have to exclude that."""
        if name == '__userDefined__':
            self.__dict__[name] = value
        else:
            self.__userDefined__[name] = value

    def __str__(self):
        """Serialize the product."""
        return msgpack.packb(self.__userDefined__)

    def _getObjectHash(self, method, data):
        """Used in getting hash. This function calls it self recursively."""
        if type(data) in (str, int, float):
            product = str(data)
        elif type(data) in (list, tuple):
            product = []
            for each in data:
                product.append(self._getObjectHash(method, each))
            product = "".join(product)
        elif type(data) == dict:
            product = []
            for each in data:
                product.append(Hash(method,
                                    self._getObjectHash(method, data[each])).hmac(each,
                                                                                  False))
            product.sort()
            product = "".join(product)
        else:
            product = str(data)
        return Hash(method, product).digest()        

    def hash(self, method='md5', raw=False):
        """Returns a hash based on values set in this instance."""
        result = self._getObjectHash(method, self.__userDefined__)
        if not raw:
            result = result.encode('hex')
        return result

    def clear(self):
        self.__dict__ = {}

if __name__ == '__main__':
    a = cipherProduct()
    a.key = [1,2,3,4,5]
    a.type = 'rsa'
    a.parameters = {'c':'d','a':'b'}

    ser = str(a)
    print len(ser)
    print a.hash(method='md5')

    b = cipherProduct(ser)
    print b.key
    print b.type
    print b.parameters
    print b.hash('md5')
