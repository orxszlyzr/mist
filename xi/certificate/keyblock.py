# -*- coding: utf-8 -*-

from ..ciphers.object import cipherProduct
from ..ciphers.asymmetric import *

class keyBlock:
    """Class for certificate key block.

       Provides functions of generating and using a key block."""
    def __init__(self, serializedString=None):
        pass

    def generate(self, keyType, **config):
        """Clear this instance for all information loaded. And generate a
           new private key basing on `config`. The private key can be
           used to derive the public pair, with `self.getPublicKey` method.

           :keyType  Must be one of `DSA`, `RSA`, `EC`. Tells which type of
                     key you are going to generate.
           :config   Parameters used to generate a key. See below.
             :bits   Optional. Only cared when `keyType` is `DSA` or `RSA`. If
                     not specified, it defaults to 1024. Must be one of the
                     followings: 1024, 2048, 3072, 4096, 8192.
             :curve  Optional. Only cared when `keyType` is `EC`. If not
                     specified, the curve will be choosen RANDOMLY(as defined
                     in `xi.ciphers.asymmetric.EC`)."""
        self.key = None    # So that a failure will not preserve previous keys,
                           # though no one should generate a new key using
                           # existing instance.
        if keyType == 'EC':
            self.key = EC.cipher()
        else:
            if not config['bits'] in [1024, 2048, 3072, 4096, 8192]:
                return False
            if keyType == 'DSA':
                self.key = DSA.cipher()
            elif keyType == 'RSA':
                self.key = RSA.cipher()
            else:
                return False
        self.key.generate(**config)
        return True

    def getPublicKey(self):
        """When this is a private key, this function will return the
           corresponding public pair of key in serialized string.
           
           Note: It is ok using Python's `str()` to get serialized key string
                 whether it's private or public. However this method can
                 derive a public key and serialize that."""
        pass

    def sign(self, source, **config):
        """Sign given input.
        
           :source  Data to be signed. Must be string. This parameter will be
                    digested using a hash function, configurable in `config`,
                    and the digest will be hashed.
           :config  Parameters used to config the signing progress. See below.
             :digestmod  A string indicating which digest mode should be used
                         to hash the given `source`. This must be one of the
                         supported methods defined in `xi.ciphers.hashes`. The
                         default value is `md5`."""
        digestmod = 'md5'
        pass

    def verify(self, source, signature):
        pass

    def encrypt(self, source):
        pass

    def decrypt(self, ciphertext):
        pass

    def id(self):
        pass

    def __str__(self):
        pass
