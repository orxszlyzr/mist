# -*- coding: utf-8 -*-

from ..ciphers.object import cipherProduct

class baseInfo:
    """Class for certificate base info.

       Provides interface to validate, hash, etc."""
    def __init__(self, load=None):
        """Initialize a baseInfo class.
           
           :load  Optional parameter, which if set will try to initialize the
                  instance with this. `load` can be either a serialized
                  baseInfo string, or a `cipherProduct` instance that has
                  got its `title` and `desc` parameters set."""
        self._storage = cipherProduct()
        if load:
            if type(load) == str:
                j = cipherProduct(load)
            else:
                j = load
            self._storage.title = j.title
            self._storage.desc = j.desc            

    def title(self, value=None):
        """Get or set `title` in the baseInfo, depends on whether `value`
           of string is set when calling."""
        if type(value) == str:
            self._storage.title = value
        else:
            if self.validate():
                return self._storage.title

    def description(self, value=None):
        """Get or set `description` in the baseInfo, depends on whether
           `value` of string is set when calling."""
        if type(value) == str:
            self._storage.desc = value
        else:
            if self.validate():
                return self._storage.desc

    def id(self):
        """Get baseInfo's ID."""
        if self.validate():
            return self._storage.hash('whirlpool')[:32]
        else:
            return False


    def validate(self):
        """Validate `title` and `description` according to rules defined for a
           baseInfo instance. This is called whenever trying to derive any
           attributes such as ID and serialized string."""
        if not self.__checkCharset(self._storage.title):
            return False
        if not self.__checkCharset(self._storage.desc):
            return False
        if len(self._storage.title) < 5 or len(self._storage.title) > 60:
            return False
        if len(self._storage.desc) > 400:
            return False
        return True

    def __checkCharset(self, string):
        """If `string` is made up with and only with all characters listed
           below. This function is case insensitive."""
        validCharset = '[abcdefghijklmnopqrstuvwxyz0123456789 .;(>_<)-#]'
        for each in string.lower():
            if not each in validCharset:
                return False
        return True

    def __str__(self):
        """Get serialized string of the baseInfo instance."""
        if self.validate():
            return str(self._storage)
        else:
            return ''
