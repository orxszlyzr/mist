# -*- coding: utf-8 -*-

from ..ciphers.object import cipherProduct
from payload import *

class signature:
    """A certificate signature object.
    
       The `signature` object carries messages, proving one who generates this
       signature is sure of such fact, that the key block being signed is
       checked and belongs to the owner's `baseinfo` field the key block
       states to be. It also carries optional authorizing messages, which may
       be useful in cases that one has to be authorized by some authority."""
       
    def __init__(self, serializedString=None):
        pass
        
    def sign(self, issuerKey, payload):
        """Clear and generate a new sign.

           After called this instance will be cleared to prepare for carrying
           new messages.

           :issuerKey   Must be a private key that can be used for signing.
                        This should be the key the one who issues carry.
           :payload     Use one type of `payload` instance as input.
        """
        pass

    def verify(self, checkKey):
        """Verify a sign.

           This could only be called in an instance which is initialized with
           `serializedString`(see __init__() for more). The payload is
           verified using given `issuerKey`. To get proper ID of the issuer's
           key, call getIssuerKeyID() function below.
             In fact, ANY error in this method will lead a value of `False`
           returned.

           :checkKey    The public key that can be used checking this
                        signature. If you provided the wrong key, the method
                        will simply return `False`."""
        pass

    def getCheckKeyID(self):
        """Get hints for retriving a key checking this signature.

           The returning value should be a string, which is the mark that
           shall have been specified along with the signature. However if this
           signature instance is not initialized, that is, either not calling
           `sign` method, or not specifing serialized string or error parsing,
           a value of `False` will be returned.
        """
        pass
