Mist
====
Core daemon providing cryptographic services.

Introduction
------------
**Mist** is a server providing cryptographic services. This depends on

* python-carrot, the messaging queue framework.
* openssl, as underlying service for encryptings.

The system plays a role similar to PGP. However, there is more:

* Up to 8192-bit RSA, and Ellptic Curve if possible.
* Trustness from signatures carry additional on authorizing.
